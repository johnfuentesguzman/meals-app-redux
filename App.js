import 'react-native-gesture-handler'; // must be ALWAYS FIRST
import React, {useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CategoriesScreen from './screens/CategoriesScreen';
import CategoriesMealsScreen from './screens/CategoriesMealsScreen';
import MealDetailScreen from './screens/MealDetailScreen';
import FavoritesScreen from './screens/FavoritesScreen';
import FiltersScreen from './screens/FiltersScreen';
import * as Font from 'expo-font';
import { AppLoading} from 'expo';
import Colors from './constants/Colors';
import TabMenu from './navigation/TabMenu';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import mealsReducer from './store/reducers/meals-Reducer';
import { Provider }  from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';


const rootReducer = combineReducers({
  mealsState: mealsReducer
});


const store = createStore(rootReducer, composeWithDevTools(
));

const fetchFonts=() => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'opens-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

const Stack = createStackNavigator();
function stackScreens({ navigation }) {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('tabPress', e => {
      e.preventDefault();
      let tabPressed = e.target.split("-")[0].toUpperCase();
      // I need to do this, because just setting the page as STACK navigitation , those page will have header
      switch (tabPressed) {
        case 'HOME':
          navigation.navigate('CategoriesScreen');
          break;
        case 'FAVORITES':
          navigation.navigate('FavoritesScreen');
          break;
        case 'FILTERS':
          navigation.navigate('FiltersScreen');
          break;
        default:
          navigation.navigate('CategoriesScreen');
          break;
      }
    });

    return unsubscribe;
  }, [navigation]);
  return (
    <Provider store={store}>
        <Stack.Navigator>
          <Stack.Screen name="CategoriesScreen" component={CategoriesScreen} options={{ title: 'Categories' }}/>
          <Stack.Screen name="CategoriesMealsScreen" component={CategoriesMealsScreen}/>
          <Stack.Screen name='MealDetailScreen' component={MealDetailScreen}/>
          <Stack.Screen name='FavoritesScreen' component={FavoritesScreen}/>
          <Stack.Screen name='FiltersScreen' component={FiltersScreen}/>
        </Stack.Navigator>
    </Provider>
  );
}

const Tab = createBottomTabNavigator();

export const App = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if(!fontsLoaded){
    return <AppLoading startAsync={fetchFonts} onFinish={()=> setFontsLoaded(true)}/>
  }
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="CategoriesScreen" // main screen
        screenOptions={({ route }) => ({
          headerStyle: {
            backgroundColor: Colors.headerBackGroundColor,
          },
          headerTintColor: Colors.headerTintColor,
          headerTitleStyle: {
            fontWeight: Colors.headerfontWeight,
          },
        })}
      >
        <Tab.Screen name="Home" component={stackScreens}  options={{
          tabBarLabel: 'Home',
          tabBarIcon:() => (
            <Ionicons name='ios-home' size={16} color='black' />
          ),
        }} />
        <Tab.Screen name="Favorites" component={stackScreens} options={{
          tabBarLabel: 'Favorites',
          tabBarIcon:() => (
            <Ionicons name='ios-star' size={16} color='black' />
          )
        }}/>
        <Tab.Screen name="Filters" component={stackScreens} options={{
          tabBarLabel: 'Filters',
          tabBarIcon:() => (
            <Ionicons name='ios-search' size={16} color='black' />
          )
        }}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
} 

export default App;