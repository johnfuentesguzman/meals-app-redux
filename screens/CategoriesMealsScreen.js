import React from 'react';
import MealList from '../components/MealList';
import {useSelector} from 'react-redux';

export const CategoriesMealsScreen = ({route,navigation}) => {
    const { selectedCategory } = route.params;
    const availableMeals = useSelector((state) => state.mealsState.filteredMeals);

    const displayMeals = availableMeals.filter(meal=> meal.categoryId.indexOf(selectedCategory.id) >= 0);

    React.useLayoutEffect(() => {
      navigation.setOptions({
        title: selectedCategory.title
      });
    }, [selectedCategory])

    return (<MealList listData={displayMeals} navigate={navigation} />)
}


  CategoriesMealsScreen.navigationOptions = (navigationData) => {
    console.log(navigationData);
  };

  export default CategoriesMealsScreen;