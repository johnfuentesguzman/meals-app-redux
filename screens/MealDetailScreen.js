import React, { useCallback, useEffect }from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CustomHeaderButton from '../components/HeaderButton';
import { HeaderButtons, Item} from 'react-navigation-header-buttons';
import {useSelector, useDispatch} from 'react-redux';
import { toogleFavorite } from '../store/actions/meals-actions';

export const MealDetailScreen = ({route, navigation}) => {
    const { selectedCategory } = route.params;
    const dispatch  = useDispatch();

    const availableMeals = useSelector((state) => state.mealsState.meals); // get all the meals
    const selectMeal = availableMeals.find(meal=> meal.id === selectedCategory);
    let favMealsSaved = useSelector((state) => state.mealsState.favoritesMeals.some((meal) =>meal.id === selectMeal.id)); // get all the meals marked as FAVORITES


    const dispatchAction = useCallback(() => {
        dispatch(toogleFavorite(selectMeal.id));
        favMealsSaved = !favMealsSaved;
    }, [dispatch, selectMeal]);

    React.useLayoutEffect(() => {
        navigation.setOptions({
          title: selectedCategory.title,
          headerRight: () => ( 
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Favorites' iconName={favMealsSaved ? 'ios-star' : 'ios-star-outline'} onPress={()=> { dispatchAction()} }/>
             </HeaderButtons>
          )

        });
      }, [selectedCategory, favMealsSaved])
  
    return (
        <View style={styles.screen}>
            <Text>{selectMeal.title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({  
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default MealDetailScreen;