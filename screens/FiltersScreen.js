import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet, Switch, Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderButtons, Item} from 'react-navigation-header-buttons'

import Colors from '../constants/Colors';
import { setFilters } from '../store/actions/meals-actions';
import HeaderButton from '../components/HeaderButton';

const FilterSwitch = (props) => {
    return (
        <View style={styles.filterContainer}>
            <Text>{props.filterText}</Text>
            <Switch trackColor={{ true: Colors.filterToogle }}
                    thumbColor={Platform.OS === 'android' ? Colors.primary : ''} 
                    value={props.valueText}
                    onValueChange={props.onChange} />
        </View>

    )
};

export const FiltersScreen = (props) => {
    const { navigation } = props;
    const dispatch = useDispatch();
    let filtersSettings = useSelector((state) => state.mealsState.filtersSettings);

    let [isGlutenFree, setIsGlutenFree] = useState(false);
    let [isLactoseFree, setIsLactoseFree] = useState(false);
    let [isVegan, setIsVegan] = useState(false);
    let [isVegetarian, setIsVegetarian] = useState(false);

    if (filtersSettings && filtersSettings.glutenFree !== undefined ){
        isGlutenFree =  filtersSettings.glutenFree;
        isLactoseFree = filtersSettings.lactoseFree;
        isVegan = filtersSettings.vegan;
        isVegetarian = filtersSettings.vegetarian;
    }

    const saveFilters = useCallback(() => {
        let appliedFilters = {
            glutenFree: isGlutenFree,
            lactoseFree: isLactoseFree,
            vegan: isVegan,
            vegetarian: isVegetarian
        };

        dispatch(setFilters(appliedFilters));
    }, [isGlutenFree, isLactoseFree, isVegan, isVegetarian, dispatch]);
    
    React.useLayoutEffect(() => {
        navigation.setOptions({
          headerRight: () => ( 
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Save"
              iconName="ios-save"
              onPress={()=> { saveFilters() }}
            />
          </HeaderButtons>
          )

        });
    })

    return (
        <View  style={styles.screen}>
            <Text style={styles.title}>Filters Availables</Text>
            <FilterSwitch valueText={isGlutenFree} filterText={'Gluten-free'} onChange={(newValue) => setIsGlutenFree(newValue)} />
            <FilterSwitch valueText={isLactoseFree} filterText={'Lactose-free'} onChange={(newValue) => setIsLactoseFree(newValue)} />
            <FilterSwitch valueText={isVegan} filterText={'Vegan'} onChange={(newValue) => setIsVegan(newValue)} />
            <FilterSwitch valueText={isVegetarian} filterText={'Vegetarian'} onChange={(newValue) => setIsVegetarian(newValue)} />
        </View>
    )
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.primary
    },
    filterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '80%',
        marginVertical: 15
    },
    title: {
        fontFamily: 'opens-sans-bold',
        fontSize: 22,
        margin: 20,
        textAlign: 'center'
    }
});
export default FiltersScreen;