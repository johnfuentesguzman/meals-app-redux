import React from 'react';
import MealList from '../components/MealList';
import { View, StyleSheet,Text } from 'react-native';
import {useSelector} from 'react-redux';

export const FavoritesScreen = (props) => {
  const {navigation} = props;
  const favMeals = useSelector(state => state.mealsState.favoriteMeals);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Favorites',
    });
  }, [favMeals])
  if (!favMeals || favMeals.length === 0) {
    return (
      <View style={styles.content}>
        <Text style={styles.title}>No favorite meals found. Start adding some!</Text>
      </View>
    );
  }

  return (
    <MealList listData={favMeals} navigate={navigation}/>
  )
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title:{
    fontFamily: 'opens-sans-bold',
    fontSize: 15,
    color: 'white',
    backgroundColor: 'rgba(0,0,0,0.5)', // opacity of black background for  recetitle
    paddingVertical: 5,
    paddingHorizontal: 12
  }
});

export default FavoritesScreen;