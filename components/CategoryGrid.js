import React from 'react'
import { View, Text, Platform, StyleSheet, TouchableOpacity, TouchableNativeFeedback } from 'react-native';

export const CategoryGrid = (props) => {
    const { data } = props;
    let TouchableComp = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version > 22) {
        TouchableComp = TouchableNativeFeedback;
    }
    return (
        <View style={styles.gridItem} >
            <TouchableComp style={{flex:1}} onPress={() => { props.onSelect() }}>
                <View style={{ ...styles.container, ...{ backgroundColor: data.item.color } }}>
                    <Text style={styles.title} numberOfLines={2}>{data.item.title}</Text>
                </View>
            </TouchableComp>
        </View>

    );
}
const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        margin: 15,
        height: 150,
        borderRadius: 10, 
        overflow: Platform.OS === 'android' && Platform.Version >= 21 ? 'hidden' : 'visible',
        elevation: 3 //  THE SAME THAN shadow property for android, shadows just work for IOS

    },
    container: {
        flex: 1,
        borderRadius: 10,
        padding: 15,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
    },
    title: {
        fontFamily: 'opens-sans-bold',
        fontSize: 15,
        textAlign: 'right' // to keep the text in case of android alined to right, becuse im using "numberOfLines 2 " to cut/wrap the text
    }
});
export default CategoryGrid;