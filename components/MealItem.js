import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TouchableNativeFeedback, ImageBackground } from 'react-native';

export const MealItem = (props) => {
    let TouchableComp = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version > 22) {
        TouchableComp = TouchableNativeFeedback;
    }
    return (
        <View style={styles.container}>
            <TouchableComp onPress={() => { props.onSelectMeal(props.id) }}>
                <View>
                    <View style={{...styles.mealRow, ...styles.mealHeader}} > 
                        <ImageBackground source={{uri: props.imageUrl}}  style={styles.imageBackground}>
                            <Text style={styles.title}>{props.title}</Text>
                        </ImageBackground>
                    </View>
                    <View style={{...styles.mealRow, ...styles.mealDetail}} >
                        <Text>{props.duration}</Text>
                        <Text>{props.complexity.toUpperCase()}</Text>
                        <Text>{props.affordability.toUpperCase()}</Text>                          
                    </View>
                </View>

            </TouchableComp>
        </View>

    )
}
const styles = StyleSheet.create({
    container:{
        height: 200,
        width: '100%',
        backgroundColor: '#f5f5f5',
        borderRadius: 10,
        overflow: 'hidden',
    },
    imageBackground:{
        width: '100%',
    },
    mealRow:{
        flexDirection: 'row'
    },
    mealHeader:{
        height: '80%'
    },
    mealDetail:{
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '20%'
    },
    title:{
        fontFamily: 'opens-sans-bold',
        fontSize: 15,
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0.5)', // opacity of black background for  recetitle
        paddingVertical: 5,
        paddingHorizontal: 12
    },
    mealText:{
        fontFamily: 'opens-sans-bold',
        fontSize: 15,
    }

});
export default MealItem;