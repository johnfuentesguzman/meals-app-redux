import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import MealItem from '../components/MealItem';


export const MealList = (props) => {
    const {listData, navigate} = props;

    const MealDetailScreen = (mealId) => {
        navigate.navigate('MealDetailScreen', {
          selectedCategory: mealId,
        })
    }

    const Item = ({listData}) =>{
        return <MealItem
                    id={listData.id}
                    imageUrl={listData.imageUrl}
                    title={listData.title}
                    complexity={listData.complexity}
                    duration={listData.duration}
                    affordability={listData.affordability}
                    onSelectMeal={(mealId) => MealDetailScreen(mealId)}
                />
    }; 

    return (
        <View style={styles.container}>
        <FlatList keyExtractor={(item) => item.id} data={listData}  renderItem={({ item }) => <Item listData={item} />} style={{width:'100%'}}></FlatList>
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
});


export default MealList;