const Colors = {
    headerBackGroundColor: '#f4511e',
    headerTintColor: '#fff',
    headerfontWeight: 'bold',
    primary: '#f9f9f9',
    blackOpacity: 'rgba(0,0,0,0.5)',
    blackOpacityAndroid: 'rgba(0,0,0,0.7)',
    filterToogle: 'rgba(83, 51, 237, 1)'
};

export default Colors; 