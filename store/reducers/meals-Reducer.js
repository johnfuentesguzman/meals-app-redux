import { MEALS, FILTERS } from '../../data/dummy-data';
import { TOOGLE_FAVORITE, SET_FILTERS} from '../actions/meals-actions';
import MealItem from '../../components/MealItem';

const initialState = {
    meals: MEALS,
    filteredMeals: MEALS,
    filtersSettings: FILTERS,
    favoritesMeals: []
}

const MealsReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOOGLE_FAVORITE:
            const existingMealIndex = state.favoritesMeals.findIndex((meal) => meal.id === action.mealId )
            if( existingMealIndex >= 0 ){ // if the id exists in the array then remove it 
                const updateFavMeals = [...state.favoritesMeals] // taking without change the origial state array (as its at that point)
                updateFavMeals.splice(existingMealIndex, 1) // removing just the id passed as a action param
                return {...state, favoritesMeals: updateFavMeals} // retuning the state with favorite array updated
            }else{
                const mealToadd = state.meals.find((meal) => meal.id === action.mealId)
                return {...state, favoritesMeals: state.favoritesMeals.concat(mealToadd)} // adding new meal to its state
            } 
            break;
        
        case SET_FILTERS:
            const appliedFilters = action.filters; // filters saved by users
            const updatedFilteredMeals = state.meals.filter((meal) => {
                if(appliedFilters.glutenFree && !meal.isGlutenFree){
                    return  false;
                }
                if(appliedFilters.lactoseFree && !meal.isLactoseFree){
                    return  false;
                }
                if(appliedFilters.vegetarian && !meal.isVegetarian){
                    return  false;
                }
                if(appliedFilters.vegan && !meal.isVegan){
                    return  false;
                }
                return true;
            });
            return {...state, filteredMeals: updatedFilteredMeals, filtersSettings: action.filters}
        
    
        default:
            return state;
    }
};

export default MealsReducer;