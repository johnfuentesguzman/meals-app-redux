class Filters {
    constructor(
      isGlutenFree,
      isVegan,
      isVegetarian,
      isLactoseFree
    ) {
      this.isGlutenFree = isGlutenFree;
      this.isVegan = isVegan;
      this.isVegetarian = isVegetarian;
      this.isLactoseFree = isLactoseFree;
    }
  }
  
  export default Filters;
  